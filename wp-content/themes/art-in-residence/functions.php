<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://www.studiopress.com/
 */

// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

// Sets up the Theme.
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function genesis_sample_localization_setup() {

	load_child_theme_textdomain( genesis_get_theme_handle(), get_stylesheet_directory() . '/languages' );

}

// Adds helper functions.
require_once get_stylesheet_directory() . '/lib/helper-functions.php';

// Adds image upload and color select to Customizer.
require_once get_stylesheet_directory() . '/lib/customize.php';

// Includes Customizer CSS.
require_once get_stylesheet_directory() . '/lib/output.php';

// Adds WooCommerce support.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';

// Adds the required WooCommerce styles and Customizer CSS.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';

// Adds the Genesis Connect WooCommerce notice.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );


/**
 * Add custom post types
 *
 * @since 2.7.0
 */

require_once get_stylesheet_directory() . '/includes/post-types/exhibitions.php';
require_once get_stylesheet_directory() . '/includes/post-types/programs.php';
require_once get_stylesheet_directory() . '/includes/post-types/visits.php';
require_once get_stylesheet_directory() . '/includes/post-types/stays.php';
require_once get_stylesheet_directory() . '/includes/components/grid.php';
require_once get_stylesheet_directory() . '/includes/components/relate-exhibitions.php';
require_once get_stylesheet_directory() . '/includes/components/stays.php';
require_once get_stylesheet_directory() . '/includes/components/artmemo-posts.php';

/**
 * Adds Gutenberg opt-in features and styling.
 *
 * @since 2.7.0
 */
function genesis_child_gutenberg_support() { // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound -- using same in all child themes to allow action to be unhooked.
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

// Registers the responsive menus.
if ( function_exists( 'genesis_register_responsive_menus' ) ) {
	genesis_register_responsive_menus( genesis_get_config( 'responsive-menus' ) );
}

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function genesis_sample_enqueue_scripts_styles() {

	$appearance = genesis_get_config( 'appearance' );

	wp_enqueue_style( // phpcs:ignore WordPress.WP.EnqueuedResourceParameters.MissingVersion -- see https://core.trac.wordpress.org/ticket/49742
		genesis_get_theme_handle() . '-fonts',
		$appearance['fonts-url'],
		[],
		null
	);

	wp_enqueue_style( 'dashicons' );

	if ( genesis_is_amp() ) {
		wp_enqueue_style(
			genesis_get_theme_handle() . '-amp',
			get_stylesheet_directory_uri() . '/lib/amp/amp.css',
			[ genesis_get_theme_handle() ],
			genesis_get_theme_version()
		);
	}

}

add_filter( 'body_class', 'genesis_sample_body_classes' );
/**
 * Add additional classes to the body element.
 *
 * @since 3.4.1
 *
 * @param array $classes Classes array.
 * @return array $classes Updated class array.
 */
function genesis_sample_body_classes( $classes ) {

	if ( ! genesis_is_amp() ) {
		// Add 'no-js' class to the body class values.
		$classes[] = 'no-js';
	}
	return $classes;
}

add_action( 'genesis_before', 'genesis_sample_js_nojs_script', 1 );
/**
 * Echo the script that changes 'no-js' class to 'js'.
 *
 * @since 3.4.1
 */
function genesis_sample_js_nojs_script() {

	if ( genesis_is_amp() ) {
		return;
	}

	?>
	<script>
	//<![CDATA[
	(function(){
		var c = document.body.classList;
		c.remove( 'no-js' );
		c.add( 'js' );
	})();
	//]]>
	</script>
	<?php
}

add_filter( 'wp_resource_hints', 'genesis_sample_resource_hints', 10, 2 );
/**
 * Add preconnect for Google Fonts.
 *
 * @since 3.4.1
 *
 * @param array  $urls          URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed.
 * @return array URLs to print for resource hints.
 */
function genesis_sample_resource_hints( $urls, $relation_type ) {

	if ( wp_style_is( genesis_get_theme_handle() . '-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = [
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		];
	}

	return $urls;
}

add_action( 'after_setup_theme', 'genesis_sample_theme_support', 9 );
/**
 * Add desired theme supports.
 *
 * See config file at `config/theme-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_theme_support() {

	$theme_supports = genesis_get_config( 'theme-supports' );

	foreach ( $theme_supports as $feature => $args ) {
		add_theme_support( $feature, $args );
	}

}

add_action( 'after_setup_theme', 'genesis_sample_post_type_support', 9 );
/**
 * Add desired post type supports.
 *
 * See config file at `config/post-type-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_post_type_support() {

	$post_type_supports = genesis_get_config( 'post-type-supports' );

	foreach ( $post_type_supports as $post_type => $args ) {
		add_post_type_support( $post_type, $args );
	}

}

// Adds image sizes.
add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'genesis-singular-images', 702, 526, true );

// Removes header right widget area.
unregister_sidebar( 'header-right' );

// Removes secondary sidebar.
unregister_sidebar( 'sidebar-alt' );

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Repositions primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

// Repositions the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
/**
 * Reduces secondary navigation menu to one level depth.
 *
 * @since 2.2.3
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' === $args['theme_location'] ) {
		$args['depth'] = 1;
	}

	return $args;

}

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;
	return $args;

}

function wpdocs_art_in_residence_scripts() {
    wp_enqueue_style( 'art-in-residence-styles',  get_stylesheet_directory_uri() . '/css/main.css');
    wp_enqueue_style( 'art-in-residence-swiper-styles',  get_stylesheet_directory_uri() . '/css/swiper-bundle.min.css');
    wp_enqueue_script( 'art-in-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js');
    wp_enqueue_script( 'art-in-residence-scripts', get_stylesheet_directory_uri() . '/js/custom.js');
    wp_enqueue_script( 'art-in-residence-swiper-scripts', get_stylesheet_directory_uri() . '/js/swiper-bundle.min.js');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_art_in_residence_scripts' );

// Add custom logo or Enable option in Customizer > Site Identity
function themename_custom_logo_setup() {
    $defaults = array(
        'height'               => 100,
        'width'                => 400,
        'flex-height'          => true,
        'flex-width'           => true,
        'header-text'          => array( 'site-title', 'site-description' ),
        'unlink-homepage-logo' => true, 
    );
 
    add_theme_support( 'custom-logo', $defaults );
}



add_theme_support( 'genesis-structural-wraps', array( 'header', 'menu-secondary', 'footer-widgets', 'footer' ) );

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    
    
    // return
    return $path;
    
}


// function customHeader() {
// 	$custom_logo_id = get_theme_mod( 'custom_logo' );
	
// 	$header_menu = wp_nav_menu( array('theme_location' => 'primary', 'container_class' => 'custom-menu-class' ) );
// 	$header = "";
// 	$header_logo = get_custom_logo();
// 	$header .= "<div class='menu_close_btn closed'></div>";
// 	$header .= $header_logo;
// 	$header .= $header_menu;
// 	$header .= "<div class='wrap hello'>";
// 	// $header .= "<div class='primry_menu_top'>";
		
// 	// 		// $header .= "<div class='custom-menu-class'><ul class='menu'><li><a href='#'>Main Nav1</a></li><li><a href='#'>Main Nav2</a></li><li><a href='#'>Main Nav3</a></li><li><a href='#'>Main Nav4</a></li></ul></div>";
			
// 	// 	$header .= "</div>";
// 	// 	$header .= "<div class='primry_menu_bottom'>";
// 	// 		$header .= "<p>Bottom</p>";	
// 	// 	$header .= "</div>";
// 	$header .= "</div>";
		
// 	echo $header;
// }

// add_action('genesis_header','customHeader');

function register_widget_areas() {

	register_sidebar( array(
	  'name'          => 'Footer area one',
	  'id'            => 'footer_area_one',
	  'description'   => 'Footer area one widget',
	  'before_widget' => '<div class="widget">',
	  'after_widget'  => '</div>',
	  'before_title'  => '<h4>',
	  'after_title'   => '</h4>',
	));

	register_sidebar( array(
		'name'          => 'Footer area two',
		'id'            => 'footer_area_two',
		'description'   => 'Footer area two widget',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	  ));
	
	  register_sidebar( array(
		'name'          => 'Footer area three',
		'id'            => 'footer_area_three',
		'description'   => 'Footer area three widget',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	  ));
	  
	  register_sidebar( array(
		'name'          => 'Footer Copyright',
		'id'            => 'footer_area_copy',
		'description'   => 'Footer copyright widget',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	  ));
	
  }

  add_action( 'widgets_init', 'register_widget_areas' );

function megaMenu()  {
	$menu_name = 'primary';
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
	$header_logo = get_custom_logo();
	$small_logo = get_stylesheet_directory_uri() . '/images/ArtInResidence_logo_icon_white.png';

	$menu = "";

	// var_dump($menuitems);

	$menu .= "<div class='menu_container'>";
		$menu .= "<div class='top_menu_container'>";
		$menu .= "<div class='menu_close_btn closed'></div>";
		$menu .= $header_logo;
			$menu .= "<ul class='primary_menu'>";
				foreach( $menuitems as $item ) {
					$title = $item->title;
					$lowerTitle = str_replace(' ', '_', strtolower($item->title));
					$link = $item->url;
					if ( !$item->menu_item_parent ) {
						$parent_id = $item->ID;
						$menu .= "<li data-id='parent_{$parent_id}' class='nav_item_{$lowerTitle}'><a href='{$link}'>{$title}</a></li>";
					}
				}
			$menu .= "</ul>";
			$menu .= "<div class='mobile_logo'><a href='/'><img src='{$small_logo}' alt='Art In Residence' /></a></div>";
		$menu .= "</div>";
		$menu .= "<div class='bottom_menu_container'>";
			$menu .= "<ul class='hidden_mobile_menu'>";
				$menu .= "<li><a href='https://art-in-residence.square.site/' target='_blank'>Donate</a></li>";
				$menu .= "<li><a href='https://artmemomagazine.com/' target='_blank'>Art Memo</a></li>";
			$menu .= "</ul>";
			$menu .= "<div class='program_menu'><ul>";
			foreach( $menuitems as $item ) {
				$title = $item->title;
				$link = $item->url;
				if ( $item->menu_item_parent ) {
					$menu .= "<li id='parent_{$item->menu_item_parent}'><a href='{$link}'>{$title}</a></li>";
				}
			}
			$menu .= "</ul></div>";
			$menu .= "<div class='info_box'>";
				$menu .= "<div class='info_box_space info_box_section'>";
					$menu .= "<p>AIR: SPACE</p>";
					$menu .= "<p>34°43’38.1″N 118°22’12.4″W</p>";
				$menu .= "</div>";
				$menu .= "<div class='info_box_cedar info_box_section'>";
					$menu .= "<p>AIR: CEDAR</p>";
					$menu .= "<p>44857 Cedar Ave<br />Lancaster, CA 93534</p>";
				$menu .= "</div>";
				$menu .= "<div class='info_box_contact info_box_section'>";
					$menu .= "<p>info@artinresidence.gallery</p>";
				$menu .= "</div>";
			$menu .= "</div>";
			
		$menu .= "</div>";
	$menu .= "</div>";

	echo $menu;

	
}
add_action('genesis_after_header','megaMenu');

remove_action( 'genesis_footer', 'genesis_do_footer' );

add_action('genesis_footer','custom_footer');

function custom_footer() {

	if ( is_active_sidebar( 'footer_area_one' ) ) : ?>

		<div id="footer_area_one" class="footer_left_container column">
	
			<?php dynamic_sidebar( 'footer_area_one' ); ?>
	
		</div><!-- #primary .aside -->
	
	<?php endif; 

	if ( is_active_sidebar( 'footer_area_two' ) ) : ?>

	<div id="footer_area_two" class="footer_middle_container column">

		<?php dynamic_sidebar( 'footer_area_two' ); ?>

	</div><!-- #primary .aside -->

	<?php endif; 

	if ( is_active_sidebar( 'footer_area_three' ) ) : ?>

	<div id="footer_area_three" class="footer_right_container column">

		<?php dynamic_sidebar( 'footer_area_three' ); ?>

	</div><!-- #primary .aside -->

	<?php endif;
}

add_action('genesis_footer','mobile_footer');

function mobile_footer() {

	if ( is_active_sidebar( 'footer_area_two' ) ||  is_active_sidebar( 'footer_area_copy' )) : ?>
	<div class='mobile_footer_column_1 mobile_footer_column'>
	
			<?php dynamic_sidebar( 'footer_area_two' ); ?>
			<?php dynamic_sidebar( 'footer_area_copy' ); ?>
	
	</div><!-- #primary .aside -->
	
	<?php endif; 

if ( is_active_sidebar( 'footer_area_three' ) ||  is_active_sidebar( 'footer_area_one' )) : ?>
	

	<div class='mobile_footer_column_2 mobile_footer_column'>
	
	<?php dynamic_sidebar( 'footer_area_three' ); ?>
			<?php dynamic_sidebar( 'footer_area_one' ); ?>
	
	</div><!-- #primary .aside -->
	
	<?php endif; 

}

add_action('genesis_after_footer','footer_copy');

function footer_copy() {
	if ( is_active_sidebar( 'footer_area_copy' ) ) : ?>

		<div id="footer_area_copy" class="footer_area_copy_container">
	
			<?php dynamic_sidebar( 'footer_area_copy' ); ?>
	
		</div><!-- #primary .aside -->
	
	<?php endif; 
}



