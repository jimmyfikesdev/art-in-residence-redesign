<?php
/*
Template Name: Exhibition Archive
*/

//* Unregister primary sidebar
unregister_sidebar( 'sidebar' );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

get_header();

$output = "";

if(have_posts()) : 
	$output .= "<div class='index_contaner'>";
	while(have_posts()) : 
		the_post();
    
		$title = get_the_title();
		$content = get_the_content();
		$date = get_field('date');
		$image = get_the_post_thumbnail_url(get_the_ID(),'full') ? get_the_post_thumbnail_url(get_the_ID(),'full') : ''; 
		$summary = get_field('summary'); 
		$link = get_field('link'); 
		$url = get_the_permalink(); 

		$output .= "<div class='index_item'>";
			$output .= "<div class='index_item_title'>";

				if($title) {
					if($date) {
						$output .= "<h3>{$title}<span>{$date}</span></h3>";
					} else {
						$output .= "<h3>{$title}</h3>";
					}
				}
			$output .= "</div>";
				if($image) {
					if($url) {
						$output .= "<a href='{$url}'><div class='image_container' style='background: url({$image}) center center no-repeat;'></div></a>";
					} else {
						$output .= "<div class='image_container' style='background: url({$image}) center center no-repeat;'></div>";
					}
				}

				if($summary || $url) {
					$output .= "<div class='index_item_content'>";
						if($summary) {
							$output .= "<p class='grid_summary'>{$summary}</p>";
						}
			
						if($url) {
							$output .= "<a href='{$url}' class='learn_more_btn'>Learn More</a>";
						}
					$output .= "</div>";
				}

				

			
		$output .= "</div>";


	endwhile;
	$output .= "</div>";
endif;

echo $output;


get_footer();
?>