<?php 

// Our custom post type function
function cpt_visits() {
 
    register_post_type( 'visits',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Visits' ),
                'singular_name' => __( 'Visits' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'visits'),
            'show_in_rest' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
 
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'cpt_visits' );