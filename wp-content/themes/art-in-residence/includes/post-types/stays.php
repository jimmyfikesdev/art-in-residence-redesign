<?php 

// Our custom post type function
function cpt_stays() {
 
    register_post_type( 'stays',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Stays' ),
                'singular_name' => __( 'Stays' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'stays'),
            'show_in_rest' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
            // This is where we add taxonomies to our CPT
            'taxonomies'          => array( 'category' ),
 
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'cpt_stays' );