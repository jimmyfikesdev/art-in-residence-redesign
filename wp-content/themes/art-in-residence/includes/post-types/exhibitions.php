<?php 

// Our custom post type function
function cpt_exhibitions() {
 
    register_post_type( 'exhibitions',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Exhibitions' ),
                'singular_name' => __( 'Exhibition' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'exhibitions'),
            'show_in_rest' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
 
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'cpt_exhibitions' );