<?php 

// Our custom post type function
function cpt_programs() {
 
    register_post_type( 'programs',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Programs' ),
                'singular_name' => __( 'Programs' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'programs'),
            'show_in_rest' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
 
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'cpt_programs' );