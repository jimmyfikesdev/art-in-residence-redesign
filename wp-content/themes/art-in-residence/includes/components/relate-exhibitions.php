<?php
function display_related_posts() {

    $output = "";
    $output .= "<div class='index_contaner'>";
    while (have_rows('related_content')) {
        the_row();
        
        //fetches the post object field. Assumes this is a single value, if they can select more than one you have to do a foreach loop here like the one in the example above
        $post_object = get_sub_field('related_post');
        
        if($post_object){
            //Fetch the image field from the carsandtrucks post
            $maintitle = get_the_title($post_object->ID);
            $title = get_field('title', $post_object->ID) ? get_field('title', $post_object->ID) : $maintitle;
            $content = get_the_content($post_object->ID);
            $date = get_field('date', $post_object->ID);
            $image = get_the_post_thumbnail_url($post_object->ID,'full') ? get_the_post_thumbnail_url($post_object->ID,'full') : ''; 
            $summary = get_field('summary', $post_object->ID) ? get_field('summary', $post_object->ID) : $content; 
            $link = get_field('link', $post_object->ID); 
            $url = get_the_permalink($post_object->ID);

        }

        $output .= "<div class='index_item'>";
			$output .= "<div class='index_item_title'>";
				if($title) {
					if($date) {
						$output .= "<h3>{$title}<span>{$date}</span></h3>";
					} else {
						$output .= "<h3>{$title}</h3>";
					}
				}
			$output .= "</div>";
				if($image) {
					if($link) {
						$output .= "<a href='{$link}'><div class='image_container' style='background-image: url({$image}) ;'></div></a>";
					} else {
						$output .= "<a href='{$url}'><div class='image_container' style='background-image: url({$image}) ;'></div></a>";
					}
				}

				if($summary || $url) {
					$output .= "<div class='index_item_content'>";
						if($summary) {
							$output .= "<p class='grid_summary'>{$summary}</p>";
						}
			
						if($link) {
							$output .= "<a href='{$link}' class='learn_more_btn'>Learn More</a>";
						} else {
							$output .= "<a href='{$url}' class='learn_more_btn'>Learn More</a>";
						}
					$output .= "</div>";
				}

				

			
		$output .= "</div>";
    } 

    $output .= "</div'>";


    echo $output;

}