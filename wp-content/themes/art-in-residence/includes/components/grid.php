<?php 



function do_grid($post_object, $learn_url, $grid_title, $swiper, $category) {

    $site_path = get_site_url();
	

	if($site_path === "http://artinresidence.test") {
		$dev_url = "http://artinresidence.test";
	} else {
		$dev_url = "https://rethunkdesign.com/artinresidence-gallery-redesign";
	}

    $post_type = $post_object;
    
    $post_category = $category;

    $output = "";

    if($post_category == "") {
        $args = array(  
            "post_type" => $post_type,
            "post_status" => "publish",
            "posts_per_page" => 20, 
            "orderby" => "date", 
            "order" => 'DESC',
        );
    } else {
        $args = array(  
            "post_type" => $post_type,
            "post_status" => "publish",
            "posts_per_page" => 20, 
            "orderby" => "date", 
            "order" => 'DESC', 
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $post_category,
                ),
            ),
        );
    }

    

    $loop = new WP_Query( $args );

    $arrow_img = get_stylesheet_directory_uri() . "/images/right-arrow.png";

    
    if ( $loop->have_posts() ) :
        $output .= "<div class='grid_container swiper {$swiper} post_type_{$post_type}'>";
        $output .= "<div class='grid_title'>{$grid_title} <a href='{$dev_url}{$learn_url}' class='title_arrow swiper-button-next'><img src='" .  $arrow_img . "' /></a></div>";
        $output .= "<div class='grid_wrapper swiper-wrapper'>";
    while ( $loop->have_posts() ) : 
        
            $loop->the_post();
            $alt_title = get_field('title');
            $title = $alt_title ? $alt_title : get_the_title(); 
            $date = get_field('date');
            $homepage_slide = get_field('homepage_slide');
            $image = get_the_post_thumbnail_url(get_the_ID(),'full') ? get_the_post_thumbnail_url(get_the_ID(),'full') : ''; 
            $summary = get_field('summary'); 
            $link = get_field('link'); 
            $url = $link ? $link : get_the_permalink();
            
            if($homepage_slide === true) {
                $output .= "<div class='grid_item swiper-slide'>";
                if($title) {
                    if($date) {
                        $output .= "<h3>{$title}<span>{$date}</span></h3>";
                    } else {
                        $output .= "<h3>{$title}</h3>";
                    }
                }

                if($swiper !== 'staysSwiper') {
                    if($image) {
                        if($url) {
                            $output .= "<a href='{$url}'><div class='image_container' style='background-image: url({$image});'></div></a>";
                        } else {
                            $output .= "<div class='image_container' style='background-image: url({$image});'></div>";
                        }
                        
                    }
                } else {
                    if($image) {
                        if($url) {
                            $output .= "<a href='{$url}' target='_blank'><img src='{$image}' /></a>";
                        } else {
                            $output .= "<img src='{$image}' />";
                        }
                        
                    }
                }
    
                
    
                if($summary) {
                    $output .= "<p class='grid_summary'>{$summary}</p>";
                }

                if($swiper !== 'staysSwiper') {
                    if($url) {
                        $output .= "<a href='{$url}' class='learn_more_btn'>Learn More</a>";
                    }
                } else {
                    if($url) {
                        $output .= "<a href='{$url}' target='_blank' class='learn_more_btn'>Learn More</a>";
                    }
                }
    
                
                    
                $output .= "</div>";
            }
                
            

            
         
    endwhile;
        
        $output .= "</div>";
        $output .= "</div>";
    endif;

    echo $output;

    wp_reset_postdata(); 

}
