<?php 



function artMemoPosts() {

    $post_type = "post";

    $output = "";

    $args = array(  
        "post_type" => $post_type,
        'category__and' => 10,
        "post_status" => "publish",
        "posts_per_page" => 20, 
        "orderby" => "date", 
        "order" => 'DESC',
    );

    $rightArrow_img = get_stylesheet_directory_uri() . "/images/right-arrow-wht.png";
    $leftArrow_img = get_stylesheet_directory_uri() . "/images/left-arrow-wht.png";

    

    $loop = new WP_Query( $args );

    
    if ( $loop->have_posts() ) :
        $output .= "<div class='art_memo_container'>";
        $output .= "<div class='wrapper '>";
        $output .= "<div class='swiper artMemoSwiper post_type_{$post_type}'>";
         $output .= "<div class='top_nav'><div class='artMemoLogo'><img src='" . get_stylesheet_directory_uri() .  "/images/artmemo_logo.png' /><span>Owned and supported by Art In Residence</span></div><div class='nav_arrows'><a href='#' class='title_arrow artMemoSwiperprev'><img src='" .  $leftArrow_img . "' /></a><a href='#' class='title_arrow artMemoSwipernext'><img src='" .  $rightArrow_img . "' /></a></div></div>";
         $output .= "<div class='artmemowrapper swiper-wrapper'>";
            while ( $loop->have_posts() ) : 
            
                $loop->the_post();
                $title = get_the_title(); 
                $author = get_the_author(); 
                $desc = wp_trim_words( get_the_content(), $num_words = 25, $more = null ); 
                $link = get_the_permalink(); 
                $output .= "<div class='grid_item swiper-slide'>";
                if($title) {
                    $output .= "<h3>{$title}</h3>";
                }

                if($author) {
                    $output .= "<p>Author: {$author}</p>";
                }


                if($desc) {
                    $output .= "<p>{$desc}</p>";
                }

                if($link) {
                    $output .= "<a href='{$link}' class='artMemoLink' target='_blank'>Read Full Review</a>";
                }
                    
                $output .= "</div>";
            endwhile;
        
        $output .= "</div>";
        $output .= "</div>";
        $output .= "</div>";
        $output .= "</div>";
    endif;

    echo $output;

    wp_reset_postdata(); 

}
