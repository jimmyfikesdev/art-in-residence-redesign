<?php
/**
 * /* Template Name: FAQ Template 
 * Genesis Framework.
 *
 * WARNING: This file is part of the core Genesis Framework. DO NOT edit this file under any circumstances.
 * Please do all modifications in the form of a child theme.
 *
 * @package Genesis\Templates
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://my.studiopress.com/themes/genesis/
 */

// Initialize Genesis.

//* Unregister primary sidebar
unregister_sidebar( 'sidebar' );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//* Remove the entry header markup (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );



function display_board() {
    $output = "";

    $page_title = get_the_title();
    $page_description = get_the_content();
    $board_section_title = get_field('title_override');
    $video = get_field('media');
    $image = get_field('image');
    
    $output .= "<div class='top_content faq'>";

        if($page_title) {
            $output .= "<h1>{$page_title}</h1>";
        }

        if($page_description) {
            $output .= $page_description;
        }
        
    $output .= "</div>";

    if( have_rows('questions') ):
        $output .= "<div class='bottom_content'>";
            while ( have_rows('questions') ) : 
                the_row(); 

                $question = get_sub_field('faq_question');
                $answer = get_sub_field('faq_answer');

                    $output .= "<div class='bottom_content_item faq'>"; 

                    if($question) {
                        $output .= "<h3>{$question}</h3>";
                    }

                    if($answer) {
                        $output .= "<p class='bottom_content_position'>{$answer}</p>";
                    }

                    $output .= "</div>"; 
            endwhile;

        $output .= "</div>";
    endif;

    echo $output;
}

add_action('genesis_after_entry_content', 'display_board', 1);

// function display_image($img) {
//     $output = "";
//     if($img) {
//         $output .= "<div class='full_image_container'>";
//         $output .= $img;
//         $output .= "</div>";
//     }
//     echo $output;
// }

// add_action('genesis_before_footer', 'display_image',2);


genesis();
