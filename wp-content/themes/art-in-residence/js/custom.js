$(document).ready(function () {

    const swiper = new Swiper('.swiper.swiperDefault', {
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 40,
                touchRatio: 0
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 40,
                touchRatio: 0
            },
        },
        // Optional parameters
        noSwiping: false,
        preventClicks: false,
        direction: 'horizontal',
        loop: true,

        // Navigation arrows
        navigation: {
            nextEl: '.swiperDefault .swiper-button-next',
            // prevEl: '.swiper-button-prev',
        },
    });

    const staysSwiper = new Swiper('.swiper.staysSwiper', {
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
                touchRatio: 0
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20,
                touchRatio: 0
            },
        },
        // Optional parameters
        noSwiping: false,
        preventClicks: false,
        direction: 'horizontal',
        loop: true,

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            // prevEl: '.swiper-button-prev',
        },
    });

    const artMemoSwiper = new Swiper('.swiper.artMemoSwiper', {
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
                touchRatio: 0
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 70,
                touchRatio: 0
            },
        },
        // Optional parameters
        noSwiping: false,
        preventClicks: false,
        direction: 'horizontal',
        loop: true,

        // Navigation arrows
        navigation: {
            nextEl: '.artMemoSwiper .artMemoSwipernext',
            prevEl: '.artMemoSwiper .artMemoSwiperprev',
        },
    });

    // Determine which main nav items have children
    var primary_menu = $(".primary_menu li");
    var sub_menu_items = $(".bottom_menu_container ul li");
    var bottom_menu_container = $('.bottom_menu_container');
    var id = "";
    var subID = "";
    var $window = $(window);
    var windowsize = $window.width();
    var top_menu_height = $('.top_menu_container').outerHeight();

    var site_menu_btn = $(".menu_close_btn");
    var mobile_menu_btn = $(".mobile_logo");
    $(site_menu_btn).on('click', function () {
        $(".menu_container .bottom_menu_container ul li.visible").removeClass("visible");
        $(this).toggleClass('closed');
        var initial_subID = '';
        $(sub_menu_items).each(function () {
            if ($(this).index() === 0) {
                initial_subID = $(this).attr('id');
            }
            if (initial_subID === $(this).attr("id")) {
                $(this).addClass("visible");
            }
        });
        $(bottom_menu_container).toggleClass("active");
        if($(bottom_menu_container).hasClass('active')) {
            $(bottom_menu_container).css('margin-bottom', top_menu_height + 10);
        } else {
            $(bottom_menu_container).css('margin-bottom', 0);
        }
    });

    // $(mobile_menu_btn).on('click', function () {
    //     $(".menu_container .bottom_menu_container ul li.visible").removeClass("visible");
    //     $(site_menu_btn).toggleClass('closed');
    //     var initial_subID = '';
    //     $(sub_menu_items).each(function () {
    //         if ($(this).index() === 0) {
    //             initial_subID = $(this).attr('id');
    //         }
    //         if (initial_subID === $(this).attr("id")) {
    //             $(this).addClass("visible");
    //         }
    //     });
    //     $(bottom_menu_container).toggleClass("active");
    //     if($(bottom_menu_container).hasClass('active')) {
    //         $(bottom_menu_container).css('margin-bottom', top_menu_height + 10);
    //     } else {
    //         $(bottom_menu_container).css('margin-bottom', 0);
    //     }
    // });

    // $(mobile_menu_btn).click(function() {
    //     $(bottom_menu_container).toggleClass("active");
    // });



    $(primary_menu).each(function () {
        id = $(this).attr('data-id');

        if($(this).hasClass('nav_item_art_memo')) {
            $(this).find('a').attr('target', '_blank')
        }
        
        if($(this).hasClass('nav_item_donate')) {
            $(this).find('a').attr('target', '_blank')
        }

        $(sub_menu_items).each(function () {
            subID = $(this).attr('id');

            if (subID === id) {
                $(".primary_menu li[data-id='" + subID + "']").addClass("has_children");
            }
        });
    });

    // Click event to activate mega menu

    var nav_with_child = $(".primary_menu li.has_children a");
    var active_id = "";
    var active_subID = "";
    

    // if (windowsize < 960) {
    //     //if the window is greater than 440px wide then turn on jScrollPane..
    //     $(".menu_container .bottom_menu_container").css('margin-bottom', top_menu_height);
    // }

    $(nav_with_child).on('click', function (e) {
        e.preventDefault();
        $(site_menu_btn).removeClass('closed');
        $(".menu_container .bottom_menu_container ul li.visible").removeClass("visible");
        active_id = $(this).closest("li").attr('data-id');
        $(sub_menu_items).each(function () {
            active_subID = $(this).attr('id');

            if (active_subID === active_id) {
                $(this).addClass("visible");
            }
        });
        $(bottom_menu_container).addClass("active");
        if($(bottom_menu_container).hasClass('active')) {
            $(bottom_menu_container).css('margin-bottom', '40px');
        } else {
            $(bottom_menu_container).css('margin-bottom', 0);
        }
    });

    // var footer_icons = $('.menu-footer-social-container .menu li');
    // $(footer_icons).each(function () {
    //     $(this).text('');
    // })


});